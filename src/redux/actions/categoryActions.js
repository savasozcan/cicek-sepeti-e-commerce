import axios from "axios";
import * as actionTypes from "../constants";

export function getCategoriesSuccess(category) {
  return {
    type: actionTypes.FETCH_CATEGORY_LIST,
    payload: category,
  };
}

export function getCategories() {
  return async (dispatch) => {
    await axios.get("http://localhost:8000/categories").then((value) => {
      dispatch(getCategoriesSuccess(value.data));
    });
  };
}
