import axios from "axios";
import * as actionTypes from '../constants'

export function getProductsSuccess(products) {
    return {
        type: actionTypes.GET_PRODUCTS_SUCCESS,
        payload: products
    }
}

export function getProducts() {
    return async dispatch => {
        await axios.get('http://localhost:8000/products').then(value => {
            dispatch(getProductsSuccess(value.data))
        })
    }
}
