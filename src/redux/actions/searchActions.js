import * as actionTypes from "../constants";

export default function getSearch(search) {
  return {
    type: actionTypes.GET_SEARCH,
    payload: search,
  };
}
