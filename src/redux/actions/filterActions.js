import * as actionTypes from "../constants";

export default function getFilter(id) {
  return {
    type: actionTypes.GET_FILTER,
    payload: id,
  };
}
