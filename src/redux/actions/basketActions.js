import * as actionTypes from "../constants";

export function addToBasket(product) {
  return {
    type: actionTypes.ADD_TO_BASKET,
    payload: product,
  };
}
