import * as actionTypes from "../constants";
const initialState = {
  basket: [],
};

export default function basketReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.ADD_TO_BASKET:
      return {
        basket: [...state.basket, action.payload],
      };
    case actionTypes.REMOVE_FROM_BASKET:
      const index = state.basket.findIndex(
        (item) => item.id === action.payload
      );
      return {
        ...state,
        basket: [...state.basket.filter((_, i) => i !== index)],
      };
    default:
      return state;
  }
}
