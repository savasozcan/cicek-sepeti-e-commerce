import { combineReducers } from "redux";
import productsReducer from "./productReducers";
import basketReducer from "./basketReducer";
import categoryReducer from "./categoryReducer";
import filterReducer from "./filterReducer";
import searchReducer from "./searchReducer";

const rootReducer = combineReducers({
  productsReducer,
  basketReducer,
  categoryReducer,
  filterReducer,
  searchReducer,
});

export default rootReducer;
