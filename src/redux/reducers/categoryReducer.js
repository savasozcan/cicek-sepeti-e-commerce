import { FETCH_CATEGORY_LIST } from "../constants";
const initialState = {
  categoryList: [],
};

export default function categoryReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CATEGORY_LIST:
      return {
        ...state,
        categoryList: action.payload,
      };
    default:
      return state;
  }
}
