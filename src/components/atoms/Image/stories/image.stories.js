import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';
import { Col, Row } from 'styled-bootstrap-grid';
import Index from '../index';
import Panel from '../../Panel/Panel';

export default {
  title: 'Atoms/Index',
  component: Index,
  decorators: [withKnobs],
};
const imgSource = 'https://dummyimage.com/300x300/000/fff.jpg';

const noAspectRatioSource = 'https://dummyimage.com/600x400/000/fff';

export const Standard = () => (
  <Panel use="container">
    <Row>
      <Col sm="6">
        <Index use="rounded" alt="image" src={imgSource} />
      </Col>
      <Col sm="6">
        <Index use="square" alt="image" src={imgSource} />
      </Col>
      <Col sm="6">
        <Index use="circle" alt="image" src={imgSource} />
      </Col>
      <Col sm="6">
        <Index use="circle" alt="image" src="wrong image" />
      </Col>
    </Row>
  </Panel>
);

export const Fixed = () => (
  <>
    <Panel use="container">
      <Row>
        <Col sm="2" md="2" lg="2" xl="1">
          <Index use="circle" fixed="small" alt="image" src={noAspectRatioSource} />
        </Col>
        <Col sm="4" md="3" lg="3" xl="2">
          <Index use="circle" fixed="medium" alt="image" src={noAspectRatioSource} />
        </Col>
        <Col sm="6" md="4" lg="4" xl="3">
          <Index use="circle" fixed="large" alt="image" src={noAspectRatioSource} />
        </Col>
        <Col sm="8" md="5" lg="5" xl="4">
          <Index use="circle" fixed="xLarge" alt="image" src={noAspectRatioSource} />
        </Col>
        <Col sm="6" md="6" lg="6" xl="5">
          <Index use="circle" fixed="xxLarge" alt="image" src={noAspectRatioSource} />
        </Col>
      </Row>
    </Panel>
    <br />
    <Panel use="container">
      <Row>
        <Col sm="2" md="2" lg="2" xl="1">
          <Index use="rounded" fixed="small" alt="image" src={noAspectRatioSource} />
        </Col>
        <Col sm="4" md="3" lg="3" xl="2">
          <Index use="rounded" fixed="medium" alt="image" src={noAspectRatioSource} />
        </Col>
        <Col sm="6" md="4" lg="4" xl="3">
          <Index use="rounded" fixed="large" alt="image" src={noAspectRatioSource} />
        </Col>
        <Col sm="8" md="5" lg="5" xl="4">
          <Index use="rounded" fixed="xLarge" alt="image" src={noAspectRatioSource} />
        </Col>
        <Col sm="6" md="6" lg="6" xl="5">
          <Index use="rounded" fixed="xxLarge" alt="image" src={noAspectRatioSource} />
        </Col>
      </Row>
    </Panel>
  </>
);
