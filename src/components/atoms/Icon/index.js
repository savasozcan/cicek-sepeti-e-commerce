import React from 'react';
import PropTypes from 'prop-types';
import FA from 'react-fontawesome';
import FeatherIcon from 'feather-icons-react';

const Icon = ({
  name, size, type, ...rest
}) => (<>
  {type === "fontawesome"
    ? <FA name={name} {...rest} style={{ fontSize: size, ...rest.style }} />
    : <FeatherIcon icon={name} size={size} {...rest} />}
</>
);

Icon.defaultProps = {
  size: 18,
  type: "feather"
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  size: PropTypes.number,
};

export default Icon;
