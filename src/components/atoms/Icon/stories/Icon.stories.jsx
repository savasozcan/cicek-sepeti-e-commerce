/* eslint-disable react/style-prop-object */
import React from 'react';
import Icon from '../index';

// This default export determines where your story goes in the story list
export default {
  title: 'Atoms/Icon',
  component: Icon,
};

const Template = (args) => (
  <div>
    <Icon {...args} />
  </div>
);

export const Default = Template.bind({});
Default.args = {
  size: 18,
  name: 'chevron-down',
  type: 'feather',
};
