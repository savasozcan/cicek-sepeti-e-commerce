/* eslint-disable react/forbid-prop-types */
import React from "react";
import PropTypes from "prop-types";
import Icon from "../Icon";

import { StyledButton } from "./styles";

const Button = ({
  use,
  align,
  children,
  prefixIcon,
  prefixIconSize,
  suffixIcon,
  suffixIconSize,
  iconPosition,
  iconType,
  ...rest
}) => {
  return (
    <StyledButton
      type="text"
      {...rest}
      use={use}
      align={align}
      hasChild={!!children}
    >
      {prefixIcon && (
        <Icon
          type="feather"
          name={prefixIcon}
          size={prefixIconSize}
          className="btn-icon"
          style={{ marginRight: children ? 12 : 0 }}
        />
      )}
      <span>{children}</span>
      {suffixIcon && children && (
        <Icon
          type="feather"
          name={suffixIcon}
          className="btn-icon"
          size={suffixIconSize}
          style={{ marginLeft: 12 }}
        />
      )}
    </StyledButton>
  );
};

Button.defaultProps = {
  use: "primary",
  children: "",
  prefixIconSize: 16,
  suffixIconSize: 16,
  align: "center",
  iconPosition: "left",
  size: "normal",
  shape: "flat",
  iconType: "fontawesome",
};

Button.propTypes = {
  use: PropTypes.oneOf([
    "primary",
    "secondary",
    "ghost",
    "text",
    "textBlack",
    "textWhite",
  ]),
  children: PropTypes.any,
  prefixIcon: PropTypes.string,
  prefixIconSize: PropTypes.number,
  suffixIcon: PropTypes.string,
  suffixIconSize: PropTypes.number,
  align: PropTypes.oneOf(["left", "center", "right"]),
  iconPosition: PropTypes.oneOf(["left", "right"]),
  size: PropTypes.oneOf(["normal", "compact"]),
  shape: PropTypes.oneOf(["flat", "circle"]),
};

export default Button;
