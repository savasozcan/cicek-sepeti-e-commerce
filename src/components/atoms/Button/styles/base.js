import styled from "styled-components";
import { Button } from "antd";

export const base = styled(Button)`
  font-weight: 700;
  font-size: ${(props) => props.theme.fontSizes.size16.size};
  letter-spacing: 0.5px;
  text-align: center;
  border-radius: ${(props) => (props.shape === "pill" ? "28px" : "4px")};
  border: 1px solid transparent;
  display: flex;
  align-items: center;
  cursor: pointer;

  &:disabled {
    opacity: 0.3;
  }

  i {
    font-size: 16px;
  }
`;
