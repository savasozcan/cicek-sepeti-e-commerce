/* eslint-disable no-nested-ternary */
import { css } from "styled-components";

export const primary = css`
  background-color: ${(props) => props.theme.colors.white} !important;
  color: ${(props) => props.theme.colors.brown} !important;
  padding: 0
    ${(props) => (props.hasChild ? props.theme.sizes.medium : 0)}!important;
  .icon-wrapper {
    svg {
      stroke: ${(props) => props.theme.colors.white} !important;

      path {
        stroke: ${(props) => props.theme.colors.white} !important;
      }
    }
  }
`;

export const grey = css`
  background-color: ${(props) => props.theme.colors.primaryGrey} !important;
  // box-shadow: 4px 8px 24px rgba(128, 57, 47, 0.2) !important;
  color: ${(props) => props.theme.colors.greyExtraDark} !important;
  padding: 0
    ${(props) => (props.hasChild ? props.theme.sizes.medium : 0)}!important;

  &:hover,
  &:focus,
  &:active {
    color: ${(props) => props.theme.colors.greyExtraDark} !important;
    border: 1px solid ${(props) => props.theme.colors.greyExtraDark} !important;
  }

  .icon-wrapper {
    svg {
      stroke: ${(props) => props.theme.colors.greyExtraDark} !important;

      path {
        stroke: ${(props) => props.theme.colors.greyExtraDark} !important;
      }
    }
  }
`;

export const secondary = css`
  background-color: ${(props) => props.theme.colors.primary10} !important;
  color: ${(props) => props.theme.colors.primary} !important;
  border: 1px solid ${(props) => props.theme.colors.primary30} !important;
  padding: 0
    ${(props) => (props.hasChild ? props.theme.sizes.medium : 0)}!important;

  &:hover {
    background-color: ${(props) => props.theme.colors.primary20} !important;
  }

  .icon-wrapper {
    svg {
      path {
        stroke: ${(props) => props.theme.colors.primary} !important;
      }
    }
  }
`;

export const ghost = css`
  background-color: ${(props) => props.theme.colors.superWhite} !important;
  color: ${(props) => props.theme.colors.blue} !important;
  border: 1px solid ${(props) => props.theme.colors.black} !important;
  border-color: ${(props) => props.theme.colors.blue} !important;
  padding: 0
    ${(props) => (props.hasChild ? props.theme.sizes.medium : 0)}!important;

  &:hover,
  &:focus,
  &:active {
    background-color: #f1f1f1 !important;
  }

  span {
    font-weight: ${(props) => props.theme.fonts.weights.regular};
  }

  .icon-wrapper {
    svg {
      stroke: ${(props) => props.theme.colors.black} !important;

      path {
        stroke: ${(props) => props.theme.colors.black} !important;
      }
    }
  }
`;

export const bordered = css`
  background-color: ${(props) => props.theme.colors.superWhite} !important;
  color: ${(props) => props.theme.colors.primary} !important;
  border: 1px solid ${(props) => props.theme.colors.primary} !important;
  border-color: ${(props) => props.theme.colors.primary} !important;
  padding: 0
    ${(props) => (props.hasChild ? props.theme.sizes.medium : 0)}!important;

  width: ${(props) => {
    console.log(props);
  }};

  &:hover,
  &:focus,
  &:active {
    background-color: #fff7f5 !important;
    color: ${(props) => props.theme.colors.primary20} !important;
    border: 1px solid ${(props) => props.theme.colors.primary20} !important;
    border-color: ${(props) => props.theme.colors.primary20} !important;
  }

  .btn-icon {
    &:first-child {
      padding-right: 12px;
      height: 32px;
      border-right: 1px solid ${(props) => props.theme.colors.primary};
      display: flex;
      align-items: center;
    }
    &:last-child {
      padding-left: 12px;
      height: 32px;
      border-left: 1px solid ${(props) => props.theme.colors.primary};
      display: flex;
      align-items: center;
    }
  }
`;

export const text = css`
  background-color: transparent !important;
  color: ${(props) => props.theme.colors.primary} !important;
  border: 1px solid transparent !important;
  border-color: transparent !important;
  box-shadow: none !important;
  &:hover,
  &:focus,
  &:active {
    border: 1px solid transparent !important;
    border-color: transparent !important;
    background-color: transparent !important;
    color: ${(props) => props.theme.colors.primary} !important;
  }

  .icon-wrapper {
    svg {
      stroke: ${(props) => props.theme.colors.primary} !important;

      path {
        stroke: ${(props) => props.theme.colors.primary} !important;
      }
    }
  }
`;

export const textBlack = css`
  background-color: transparent !important;
  color: ${(props) => props.theme.colors.black} !important;
  border: 1px solid transparent !important;
  border-color: transparent !important;
  box-shadow: none !important;
  &:hover,
  &:focus,
  &:active {
    background-color: transparent !important;
    color: ${(props) => props.theme.colors.primary} !important;
    border: 1px solid transparent !important;
    border-color: transparent !important;

    .icon-wrapper {
      svg {
        stroke: ${(props) => props.theme.colors.primary} !important;

        path {
          stroke: ${(props) => props.theme.colors.primary} !important;
        }
      }
    }
  }

  .icon-wrapper {
    svg {
      stroke: ${(props) => props.theme.colors.black} !important;

      path {
        stroke: ${(props) => props.theme.colors.black} !important;
      }
    }
  }
`;

export const textWhite = css`
  background-color: transparent !important;
  color: ${(props) => props.theme.colors.superWhite} !important;
  border: 1px solid transparent !important;
  border-color: transparent !important;
  box-shadow: none !important;
  &:hover,
  &:focus,
  &:active {
    background-color: transparent !important;
    color: ${(props) => props.theme.colors.primary} !important;
    border: 1px solid transparent !important;
    border-color: transparent !important;

    .icon-wrapper {
      svg {
        stroke: ${(props) => props.theme.colors.primary} !important;

        path {
          stroke: ${(props) => props.theme.colors.primary} !important;
        }
      }
    }
  }

  .icon-wrapper {
    svg {
      stroke: ${(props) => props.theme.colors.superWhite} !important;

      path {
        stroke: ${(props) => props.theme.colors.superWhite} !important;
      }
    }
  }
`;

export const basket = css`
  background-color: ${(props) => props.theme.colors.blue};
  color: ${(props) => props.theme.colors.white};
  padding: 15px 25px !important;
  font-weight: ${(props) => props.theme.fonts.weights.regular} !important;
  font-size: ${(props) => props.theme.sizes.medium}!important;

  &:hover {
    color: ${(props) => props.theme.colors.white};
  }
`;

export const search = css`
  background-color: ${(props) => props.theme.colors.green};
  color: ${(props) => props.theme.colors.white};
  padding: 20px 25px !important;
  font-weight: ${(props) => props.theme.fonts.weights.regular} !important;
  font-size: ${(props) => props.theme.fontSizes.size18.size}!important;

  &:hover {
    background-color: ${(props) => props.theme.colors.green};
    color: ${(props) => props.theme.colors.white};
  }
`;

export const categoryGhost = css`
  background-color: ${(props) => props.theme.colors.superWhite} !important;
  color: ${(props) => props.theme.colors.greyLight} !important;
  border: 1px solid ${(props) => props.theme.colors.lightGrey} !important;
  border-radius: ${(props) => props.theme.fontSizes.size10.size};
  padding: 0
    ${(props) => (props.hasChild ? props.theme.sizes.medium : 0)}!important;
  font-size: ${(props) => props.theme.fontSizes.size18.size};
`;

export const categoryAll = css`
  background-color: ${(props) => props.theme.colors.blue};
  border-radius: ${(props) => props.theme.fontSizes.size10.size};
  color: ${(props) => props.theme.colors.white};
  padding: 15px 25px !important;
  font-weight: ${(props) => props.theme.fonts.weights.regular} !important;
  font-size: ${(props) => props.theme.sizes.medium}!important;

  &:hover {
    background-color: ${(props) => props.theme.colors.blue};
    color: ${(props) => props.theme.colors.white};
  }
`;
