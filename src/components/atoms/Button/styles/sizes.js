/* eslint-disable no-nested-ternary */
import { css } from 'styled-components';

export const normal = css`
    padding: 0 ${(props) => (props.hasChild ? props.theme.sizes.xsmall : 0)};
    font-weight: 600;
    font-size: ${(props) => props.theme.fontSizes.size14.size};
    line-height: 20px;
    height: ${(props) => props.theme.button.height};
    width: ${(props) => (props.hasChild ? 'auto' : '32px')};

    .icon-wrapper {
        width: 18px;
        height: 18px;
        margin-right: ${(props) => (!props.hasChild ? '0' : props.iconPosition === 'left' ? '10px' : 0)}; 
        margin-left: ${(props) => (!props.hasChild ? '0' : props.iconPosition === 'right' ? '10px' : 0)};
        
        svg {
            width: 18px;
            height: 18px;
        }
`;

export const compact = css`
    padding: 0 ${(props) => (props.hasChild ? props.theme.sizes.xsmall : 0)};
    font-weight: 600;
    font-size: ${(props) => props.theme.fontSizes.size14.size};
    line-height: 20px;
    height: ${(props) => props.theme.button.height};
    width: ${(props) => (props.hasChild ? 'auto' : '32px')};

    .icon-wrapper {
        width: 18px;
        height: 18px;
        margin-right: ${(props) => (!props.hasChild ? '0' : props.iconPosition === 'left' ? '10px' : 0)}; 
        margin-left: ${(props) => (!props.hasChild ? '0' : props.iconPosition === 'right' ? '10px' : 0)};
        
        svg {
            width: 18px;
            height: 18px;
        }
    }
`;

export const large = css` 
    padding: 0 ${(props) => (props.hasChild ? props.theme.sizes.large : 0)};
    line-height: 22px;
    height: 48px;
    width: ${(props) => (props.hasChild ? 'auto' : '48px')};
`;
