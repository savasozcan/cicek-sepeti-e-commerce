import styled from "styled-components";
import variant from "../../../../utils/variant";

import { base } from "./base";
import {
  primary,
  secondary,
  ghost,
  textBlack,
  textWhite,
  text,
  grey,
  bordered,
  basket,
  search,
  categoryGhost,
  categoryAll
} from "./variants";
import { normal, compact, large } from "./sizes";
import { left, center, right } from "./addOns";

const use = variant("use", {
  primary,
  secondary,
  ghost,
  textBlack,
  textWhite,
  text,
  grey,
  bordered,
  basket,
  search,
  categoryGhost,
  categoryAll
});

const size = variant("size", { compact, normal, large });
const align = variant("align", { left, center, right });

export const StyledButton = styled(base)(use, size, align);
