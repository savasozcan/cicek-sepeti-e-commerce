import { css } from 'styled-components';

export const left = css`
    justify-content: flex-start;
`;

export const center = css`
    justify-content: center;
`;

export const right = css`
    justify-content: flex-end;
`;
