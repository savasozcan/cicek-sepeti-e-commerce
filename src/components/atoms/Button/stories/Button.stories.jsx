/* eslint-disable react/style-prop-object */
import React from "react";
import Button from "../index";

export default {
  title: "Atoms/Button",
  component: Button,
  argTypes: {
    use: {
      control: {
        type: "select",
        options: [
          "primary",
          "secondary",
          "ghost",
          "text",
          "textBlack",
          "textWhite",
          "grey",
          "bordered",
          "basket",
          "search",
          "categoryGhost",
          "categoryAll",
        ],
      },
    },
    align: {
      control: { type: "select", options: ["left", "center", "right"] },
    },
    prefixIcon: { control: { type: "text" } },
    suffixIcon: { control: { type: "text" } },
    size: { control: { type: "select", options: ["normal", "compact"] } },
    iconPosition: { control: { type: "select", options: ["left", "right"] } },
    shape: { control: { type: "select", options: ["flat", "pill"] } },
  },
};

const TemplateWithChild = (args) => (
  <div>
    <Button {...args}>Kayıt Ol</Button>
  </div>
);

const TemplateWithoutChild = (args) => (
  <div>
    <Button {...args} />
  </div>
);

export const Primary = TemplateWithChild.bind({});
Primary.args = {
  use: "primary",
  disabled: false,
  block: false,
  size: "normal",
  align: "center",
  shape: "flat",
};

export const Secondary = TemplateWithChild.bind({});
Secondary.args = {
  use: "secondary",
  disabled: false,
  block: false,
  size: "normal",
  align: "center",
  shape: "flat",
};

export const Compact = TemplateWithChild.bind({});
Compact.args = {
  use: "secondary",
  disabled: false,
  prefixIcon: "chevron-down",
  block: false,
  size: "compact",
  align: "center",
  shape: "flat",
};

export const OnlyIcon = TemplateWithoutChild.bind({});
OnlyIcon.args = {
  use: "secondary",
  disabled: false,
  prefixIcon: "chevron-down",
  block: false,
  size: "compact",
  align: "center",
  shape: "flat",
};
