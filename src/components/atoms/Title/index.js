import React from "react";
import PropTypes from "prop-types";

import { StyledTitle } from "./styles";

const Title = ({ title, button, ...rest }) => (
  <StyledTitle {...rest}>
    {title}
    <div style={{ float: "right" }}>{button}</div>
  </StyledTitle>
);

Title.defaultProps = {
  title: "",
};

Title.propTypes = {
  title: PropTypes.string,
};

export default Title;
