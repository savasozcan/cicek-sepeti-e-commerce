/* eslint-disable react/style-prop-object */
import React from "react";
import Title from "..";
import AppThemeProvider from "../../../../AppThemeProvider/AppProvider";

export default {
  title: "Atoms/Title",
  component: Title,
};

const Template = (args) => (
  <AppThemeProvider>
    <Title {...args} />
  </AppThemeProvider>
);

export const Default = Template.bind({});
Default.args = {
  title: "Panel Title",
};
