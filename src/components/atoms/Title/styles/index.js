import styled from 'styled-components';

export const StyledTitle = styled.div`
    font-weight: bold;
    font-size: 18px;
    line-height: 22px;
    letter-spacing: 0.5px;
    border-bottom: 1px solid #E3E3E3;
    padding-bottom: 8px;
    margin-bottom: 16px;
`;
