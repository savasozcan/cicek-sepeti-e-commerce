import React from "react";
import Header from "../index";
import { Container, Row, Col } from "react-bootstrap";

export default {
  title: "Organisms/Header",
  component: Header,
};

const Template = (args) => (
  <Container>
    <Row>
      <Col>
        <Header {...args} />
      </Col>
    </Row>
  </Container>
);

export const Default = Template.bind({});
Default.args = {};
