import React from "react";
import { connect } from "react-redux";
import { Input } from "antd";
import Image from "../../atoms/Image";
import Basket from "../../molecules/Basket";
import getSearch from "../../../redux/actions/searchActions";
import {
  StyledHeader,
  StyledWrapper,
  StyledContainer,
  StyledLogoWrapper,
  StyledSearchWrapper,
  StyledBasketContainer,
  StyledLeft,
  StyledContainerWrapper,
} from "./styles";
import logo from "../../../assets/images/logo.svg";
import Button from "../../atoms/Button";
import HeaderBottom from "../../molecules/HeaderBottom";
import enums from "./enums";
import { debounce } from "../../../utils/debounce";
import DiscountProgress from "../../molecules/DiscountProgress";

const Header = (props) => {
  const basketItems = props.basket.basket;

  const handleSearch = (search) => {
    props.getSearch(search);
  };

  const getSearchedProduct = debounce((e) => {
    handleOnChange(e);
  }, enums.SEARCH_DEBOUNCE);

  const handleOnChange = (e) => {
    e.target.value.length > enums.SEARCH_LIMIT
      ? handleSearch(e.target.value)
      : handleSearch("");
  };

  return (
    <>
      <StyledHeader>
        <StyledContainerWrapper>
          <StyledWrapper>
            <StyledContainer>
              <StyledLeft>
                <StyledLogoWrapper>
                  <Image src={logo} alt="logo" />
                </StyledLogoWrapper>
                <StyledSearchWrapper>
                  <Input
                    type="text"
                    placeholder={enums.INPUT_PLACEHOLDER}
                    onChange={(e) => getSearchedProduct(e)}
                  />
                  <Button use="search" shape="pill">
                    {enums.SEARCH_BUTTON_TEXT}
                  </Button>
                </StyledSearchWrapper>
              </StyledLeft>

              <StyledBasketContainer>
                <Basket items={basketItems} />
                <DiscountProgress />
              </StyledBasketContainer>
            </StyledContainer>
          </StyledWrapper>
        </StyledContainerWrapper>
      </StyledHeader>
      <HeaderBottom />
    </>
  );
};

const mapStateToProps = (state) => ({
  basket: state.basketReducer,
  products: state.productsReducer,
  search: state.searchReducer,
});

const mapDispatchToProps = {
  getSearch,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
