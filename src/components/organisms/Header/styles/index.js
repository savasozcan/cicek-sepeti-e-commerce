import styled from "styled-components";
import { Container } from "styled-bootstrap-grid";

export const StyledHeader = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  flex-flow: row nowrap;
  justify-content: flex-start;
`;

export const StyledWrapper = styled.div`
  padding: 20px 0;
`;
export const StyledContainer = styled.div`
  display: flex;
  padding: 0 15px;
`;
export const StyledLogoWrapper = styled.div`
  margin-right: 78px;
`;
export const StyledSearchWrapper = styled.div`
  position: relative;
  width: 60%;
  img {
    position: absolute;
    left: 20px;
    top: 50%;
    transform: translateY(-50%);
  }
  input {
    background: #f7f7f7;
    background-size: contain;
    background-repeat: no-repeat;
    border: 2px solid #edf1f2;
    border-radius: 28px;
    width: 100%;
    padding: 16px 44px;
    outline: none;
    color: #555555;
    &::placeholder {
      color: #555555;
      font-size: 16px;
    }
  }
  button {
    position: absolute;
    right: 7px;
    top: 50%;
    transform: translateY(-50%);
    border: 2px solid #edf1f2;
    border-radius: 28px;
    text-align: center;
    background-color: #51b549;
    padding: 7px 21px;
    font-size: 18px;
    color: #ffffff;
  }
`;
export const StyledBasketContainer = styled.div`
  display: inline-block;
  position:relative;
  background: ${(props) => props.theme.colors.blue};
  padding: 11px 10px;
  border-radius: 28px;
  color: white;
`;
export const StyledLeft = styled.div`
  display: flex;
  align-items: center;
  flex: 1 1;
`;

export const StyledContainerWrapper = styled(Container)`
  @media (min-width: 1441px) {
    max-width: 1480px;
  }
`;
