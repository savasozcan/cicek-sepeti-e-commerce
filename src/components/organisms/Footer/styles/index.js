import styled from "styled-components";

export const StyledWrapper = styled.div`
  margin-block: 30px 100px;
  border-radius: 100px;
  height: 200px;
  background-color: ${(props) => props.theme.colors.white};
  //padding: 30px;
  position: relative;
  display: flex;
  flex-direction: column;
`;
export const StyledFooterTopWrapper = styled.div`
  display: flex;
  width: 100%;
`;
export const StyledFooterMobile = styled.div`
  flex: 1 1;
  width: 30%;
`;
export const StyledInfo = styled.div`
  display: flex;
  align-items: center;
  margin-top: 35px;
  margin-bottom: 20px;
`;
export const StyledTopText = styled.p`
  margin-bottom: 5px;
  font-size: 18px;
`;
export const StyledBottomText = styled.p`
  margin-bottom: 5px;
  font-size: 16px;
`;
export const StyledPlatforms = styled.div`
  img {
    margin-right: 10px;
  }
`;
export const StyledImageWrapper = styled.div`
  flex: 1 1;
  width: 30%;

  img {
    position: absolute;
    bottom: 0px;
    width: 300px;
    left: 20%;
  }
`;

export const StyledQRWrapper = styled.div`
  padding-inline: 10px;
  text-align: left;

  img {
    vertical-align: middle;
    border-style: none;
  }
`;

export const StyledTextWrapper = styled.div`
  padding-inline: 10px;
  text-align: left;
`;
