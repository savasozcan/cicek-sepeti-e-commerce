import React from "react";
import { Container, Row, Col } from "styled-bootstrap-grid";

import phone from "../../../assets/images/footer-app.png";
import qr_code from "../../../assets/images/qr_code.png";
import app_store from "../../../assets/images/app_store.svg";
import google_play from "../../../assets/images/google_play.svg";
import {
  StyledFooterMobile,
  StyledFooterTopWrapper,
  StyledInfo,
  StyledPlatforms,
  StyledTopText,
  StyledBottomText,
  StyledWrapper,
  StyledImageWrapper,
  StyledQRWrapper,
  StyledTextWrapper,
} from "./styles";
import FooterMain from "../../molecules/Footer/FooterMain";
import FooterBottomInfo from "../../molecules/Footer/FooterBottomInfo";

const Footer = () => {
  return (
    <StyledWrapper>
      <StyledFooterTopWrapper>
        <StyledImageWrapper>
          <img src={phone} alt="" />
        </StyledImageWrapper>
        <StyledFooterMobile>
          <StyledInfo>
            <StyledQRWrapper>
              <img src={qr_code} alt="" />
            </StyledQRWrapper>
            <StyledTextWrapper>
              <StyledTopText>
                <strong>Çiçek Sepeti Mobil Uygulamayı İndirin</strong>
              </StyledTopText>
              <StyledBottomText>
                Mobil Uygulamayı QR Kod ile İndirin.
              </StyledBottomText>
            </StyledTextWrapper>
          </StyledInfo>
          <StyledPlatforms>
            <img src={google_play} alt="google play" />
            <img src={app_store} alt="app store" />
          </StyledPlatforms>
        </StyledFooterMobile>
      </StyledFooterTopWrapper>
    </StyledWrapper>
  );
};

export default Footer;
