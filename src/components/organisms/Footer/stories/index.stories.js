import React from "react";
import Footer from "../index";
import { Container, Row, Col } from 'react-bootstrap';

export default {
  title: "Organisms/Footer",
  component: Footer,
};

const Template = (args) => (
    <Container>
      <Row>
        <Col>
          <Footer {...args} />
        </Col>
      </Row>
    </Container>
);

export const Default = Template.bind({});
Default.args = {};
