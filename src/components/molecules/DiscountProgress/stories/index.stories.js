import React from "react";
import DiscountProgress from "../index";

export default {
  title: "Molecules/DiscountProgress",
  component: DiscountProgress,
};

const Template = (args) => <DiscountProgress {...args} />;

export const Default = Template.bind({});
Default.args = {};
