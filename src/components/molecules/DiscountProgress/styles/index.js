import styled from "styled-components";

export const StyledWrapper = styled.div`
  background-color: ${(props) => props.theme.colors.red};
  border-radius: 0.5rem;
  padding: ${(props) => props.theme.fontSizes.size12.size};
  position: absolute;
  top: 75px;
  right: 50px;
  font-size: 15px;
  color: ${(props) => props.theme.colors.white};
  font-weight: ${(props) => props.theme.fonts.weights.semiBold};
  width: 300px;

  &:before {
    content: "";
    display: block;
    width: 0;
    height: 0;
    position: absolute;
    border-right: 8px solid transparent;
    border-top: 8px solid transparent;
    border-bottom: 8px solid ${(props) => props.theme.colors.red};
    right: 50px;
    top: -15px;
    border-left: 8px solid transparent;
  }
`;
