import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Progress } from "antd";
import { StyledWrapper } from "./styles";
import flash from "../../../assets/images/flash.svg";

let price;
const DiscountProgress = (props) => {
  const basket = props.basket.basket;
  const [progressPercent, setProgressPercent] = useState();

  console.log("basket => ", basket);

  useEffect(() => {
    setProgressPercent(
      (basket
        ?.reduce((acc, product) => acc + Number(product.price), 0)
        .toFixed(3) *
        100) /
        500
    );
    price = (
      500 -
      basket
        ?.reduce((acc, product) => acc + Number(product.price), 0)
        .toFixed(2)
    ).toFixed(2);
  }, [basket]);

  return (
    <>
      {progressPercent > 0 && (
        <StyledWrapper>
          <img src={flash} alt="flash-icon" />
          {progressPercent <= 100 ? (
            <>
              <span>{price}</span>
              <span> ürün daha ekleyin kargo bedava</span>
            </>
          ) : (
            <span>Kargonuz bedava!!</span>
          )}
          {progressPercent <= 100 && (
            <Progress
              percent={progressPercent}
              strokeColor="#FFCE00"
              strokeWidth={5}
              trailColor="#D01D32"
              showInfo={false}
            />
          )}
        </StyledWrapper>
      )}
    </>
  );
};

const mapStateToProps = (state) => ({
  basket: state.basketReducer,
});

export default connect(mapStateToProps)(DiscountProgress);
