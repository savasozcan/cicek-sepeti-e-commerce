import React from "react";
import FooterCopyRight from "../index";
import { Container, Row, Col } from "react-bootstrap";

export default {
  title: "Molecules/Footer/FooterCopyRight",
  component: FooterCopyRight,
};

const Template = (args) => (
  <Container>
    <Row>
      <Col>
        <FooterCopyRight {...args} />
      </Col>
    </Row>
  </Container>
);

export const Default = Template.bind({});
Default.args = {};
