import React from "react";
import { StyledWrapper } from "./styles";

const FooterCopyRight = () => {
  return (
    <StyledWrapper>
      <p>Copyright © 2019 Çiçek Sepeti İnternet Hizmetleri A.Ş</p>
    </StyledWrapper>
  );
};

export default FooterCopyRight;
