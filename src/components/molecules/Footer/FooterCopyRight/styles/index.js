import styled from "styled-components";

export const StyledWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 60px;
  background-color: ${(props) => props.theme.colors.greyLighter};
  p {
    color: ${(props) => props.theme.colors.black};
    font-size: ${(props) => props.theme.sizes.xsmall};
    font-family: "Source Sans Pro", sans-serif;
  }
`;
