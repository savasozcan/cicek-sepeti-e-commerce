import React from "react";
import { Row, Col } from "styled-bootstrap-grid";
import {
  StyledWrapper,
  StyledInfoWrapper,
  StyledSocialWrapper,
  StyledLeftWrapper,
  StyledFooterList,
  StyledItemTitle
} from "./styles";
import logo from "../../../../assets/images/logo.svg";
import youtube from "../../../../assets/images/youtube.svg";
import twitter from "../../../../assets/images/twitter.svg";
import facebook from "../../../../assets/images/facebook.svg";
import instagram from "../../../../assets/images/instagram.svg";
import app from "../../../../assets/images/app.svg";

const FooterMain = () => {
  return (
    <StyledWrapper>
      <Row>
        <Col md={10}>
          <StyledLeftWrapper>
            <StyledInfoWrapper>
              <img src={logo} alt="logo" />
            </StyledInfoWrapper>
            <StyledSocialWrapper>
              <img src={facebook} alt="logo" />
              <img src={twitter} alt="logo" />
              <img src={instagram} alt="logo" />
              <img src={youtube} alt="logo" />
              <img src={app} alt="logo" />
            </StyledSocialWrapper>
            <p>
              CicekSepeti.com olarak kişisel verilerinizin gizliliğini
              önemsiyoruz. 6698 sayılı Kişisel Verilerin Korunması Kanunu
              kapsamında oluşturduğumuz aydınlatma metnine buradan
              ulaşabilirsiniz.
            </p>
          </StyledLeftWrapper>
        </Col>
        <Col md={14}>
          <div>
            <StyledFooterList>
              <li>
                <StyledItemTitle>Faydalı Bilgiler</StyledItemTitle>
                <ul>
                  <li>
                    <a href="/#">Çiçek Bakımı</a>
                  </li>
                  <li>
                    <a href="/#">Çiçek Eşliğinde Notlar</a>
                  </li>
                  <li>
                    <a href="/#">Çiçek Anlamları</a>
                  </li>
                  <li>
                    <a href="/#">Özel Günler</a>
                  </li>
                  <li>
                    <a href="/#">Mevsimlere Göre Çiçekler</a>
                  </li>
                  <li>
                    <a href="/#">BonnyFood Saklama Koşulları</a>
                  </li>
                  <li>
                    <a href="/#">Site Haritası</a>
                  </li>
                </ul>
              </li>
              <li>
                <StyledItemTitle>Kurumsal</StyledItemTitle>
                <ul>
                  <li>
                    <a href="/#">Hakkımızda</a>
                  </li>
                  <li>
                    <a href="/#">Kariyer</a>
                  </li>
                  <li>
                    <a href="/#">ÇiçekSepeti’nde Satış Yap</a>
                  </li>
                  <li>
                    <a href="/#">Kurumsal Müşterilerimiz</a>
                  </li>
                  <li>
                    <a href="/#">Reklamlarımız</a>
                  </li>
                  <li>
                    <a href="/#">Basında Biz</a>
                  </li>
                  <li>
                    <a href="/#">Kampanyalar</a>
                  </li>
                  <li>
                    <a href="/#">Vizyonumuz</a>
                  </li>
                </ul>
              </li>
              <li>
                <StyledItemTitle>İletişim</StyledItemTitle>
                <ul>
                  <li>
                    <a href="/#">Bize Ulaşın</a>
                  </li>
                  <li>
                    <a href="/#">Sıkça Sorulan Sorular</a>
                  </li>
                </ul>
              </li>
              <li>
                <StyledItemTitle>Gizlilik Sözleşmesi</StyledItemTitle>
                <ul>
                  <li>
                    <a href="/#">Mesafeli Satış Sözleşmesi</a>
                  </li>
                  <li>
                    <a href="/#">Bilgi Toplumu Hizmetleri</a>
                  </li>
                  <li>
                    <a href="/#">Gizlilik Sözleşmesi</a>
                  </li>
                  <li>
                    <a href="/#">Ödeme Seçenekleri</a>
                  </li>
                  <li>
                    <a href="/#">Hesap Bilgileri</a>
                  </li>
                </ul>
              </li>
            </StyledFooterList>
          </div>
        </Col>
      </Row>
    </StyledWrapper>
  );
};

export default FooterMain;
