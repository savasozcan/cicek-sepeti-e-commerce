import styled from "styled-components";

export const StyledWrapper = styled.div`
  display: flex;
  font-family: "Source Sans Pro", sans-serif;
  s p {
    color: ${(props) => props.theme.colors.black};
    font-size: ${(props) => props.theme.sizes.small};
    font-family: "Source Sans Pro", sans-serif;
  }
`;

export const StyledInfoWrapper = styled.div``;

export const StyledLeftWrapper = styled.div`
  text-align: left;
`;

export const StyledSocialWrapper = styled.div`
  img {
    margin: 15px 30px 15px 0;
  }
`;

export const StyledFooterList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  list-style-type: none;
  text-align: center;
  font-family: "Source Sans Pro", sans-serif;
  border-left: 1px dashed #9b9b9b;

  li {
    list-style-type: none;
    flex: 0 0 25%;

    a {
      color: #044dc3;
      font-weight: 600;
      display: block;
      margin-bottom: 10px;
    }
    ul {
      display: contents;
      font-size: 14px;

      li {
        list-style-type: none;
        flex: 0 0 25%;
        padding-bottom: 10px;

        a {
          text-decoration: none;
          font-weight: 400;
          color: #000000;
        }
      }
    }
  }
`;

export const StyledItemTitle = styled.a`
  color: #044dc3 !important;
  font-size: 18px;
`;
