import React from "react";
import FooterMain from "../index";
import { Container, Row, Col } from "react-bootstrap";

export default {
  title: "Molecules/Footer/FooterMain",
  component: FooterMain,
};

const Template = (args) => (
  <Container>
    <Row>
      <Col>
        <FooterMain {...args} />
      </Col>
    </Row>
  </Container>
);

export const Default = Template.bind({});
Default.args = {};
