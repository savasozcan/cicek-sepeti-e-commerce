import styled from "styled-components";

export const StyledWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  padding: 50px 0;
  p {
    color: ${(props) => props.theme.colors.black};
    font-size: ${(props) => props.theme.sizes.xsmall};
    font-family: "Source Sans Pro", sans-serif;
  }
`;
