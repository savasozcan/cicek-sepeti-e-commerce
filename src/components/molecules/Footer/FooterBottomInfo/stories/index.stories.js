import React from "react";
import FooterBottomInfo from "../index";
import { Container, Row, Col } from "react-bootstrap";

export default {
  title: "Molecules/Footer/FooterBottomInfo",
  component: FooterBottomInfo,
};

const Template = (args) => (
  <Container>
    <FooterBottomInfo {...args} />
  </Container>
);

export const Default = Template.bind({});
Default.args = {};
