import React from "react";
import HeaderBottom from "../";

export default {
  title: "Molecules/HeaderBottom",
  component: HeaderBottom,
};

const Template = () => (
  <div>
    <HeaderBottom />
  </div>
);

export const Default = Template.bind({});
