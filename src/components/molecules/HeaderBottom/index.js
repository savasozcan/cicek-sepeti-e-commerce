import React from "react";
import { StyledWrapper, StyledText, StyledContainer } from "./styles";
import enums from "./enums";

const HeaderBottom = () => {
  return (
    <StyledWrapper>
      <StyledContainer>
        <StyledText>
          <span>{enums.HEADER_BOTTOM_TEXT}</span>
        </StyledText>
      </StyledContainer>
    </StyledWrapper>
  );
};

export default HeaderBottom;
