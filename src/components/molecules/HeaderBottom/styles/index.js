import styled from "styled-components";
import bg from "../../../../assets/images/bg.png";
import { Container } from "styled-bootstrap-grid";

export const StyledWrapper = styled.div`
  display: flex;
  align-items: center;
  background-image: url(${bg});
  height: 74px;
`;
export const StyledText = styled.div`
  font-size: ${(props) => props.theme.sizes.xxxlarge};
  font-family: "Source Sans Pro", sans-serif;
  color: ${(props) => props.theme.colors.white};
`;

export const StyledContainer = styled(Container)`
  @media (min-width: 1441px) {
    max-width: 1480px;
  }
`;
