import React from "react";
import {
  StyledWrapper,
  StyledImageWrapper,
  StyledTextWrapper,
  StyledInfo,
  StyledButton,
} from "./styles";
import Button from "../../atoms/Button";

const OpportunityCard = ({ data }) => {
  return (
    <StyledWrapper bgColor={data.bg}>
      <StyledImageWrapper>
        <img src={data.img} alt={data.content} />
      </StyledImageWrapper>
      <StyledTextWrapper>
        <StyledInfo>{data.content}</StyledInfo>
        <StyledButton>
          <Button use="primary" shape="pill">
            {data.buttonText}
          </Button>
        </StyledButton>
      </StyledTextWrapper>
    </StyledWrapper>
  );
};

export default OpportunityCard;
