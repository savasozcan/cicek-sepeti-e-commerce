import styled from "styled-components";

export const StyledWrapper = styled.div`
  width: 480px;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  background: ${(props) => props.bgColor};
  padding: ${(props) => props.theme.fontSizes.size10.size};
  border-radius: ${(props) => props.theme.fontSizes.size10.size};
  margin-right: ${(props) => props.theme.sizes.xmedium};
`;
export const StyledImageWrapper = styled.div`
  img {
    width: 165px;
    margin-right: ${(props) => props.theme.sizes.xxxlarge};
  }
`;
export const StyledTextWrapper = styled.div``;
export const StyledInfo = styled.div`
  color: ${(props) => props.theme.colors.greyLight};
  font-size: ${(props) => props.theme.sizes.large};
  font-family: "Source Sans Pro", sans-serif;
  font-weight: ${(props) => props.theme.fonts.weights.regular};
  margin-bottom: ${(props) => props.theme.fontSizes.size10.size};
`;
export const StyledButton = styled.div``;
