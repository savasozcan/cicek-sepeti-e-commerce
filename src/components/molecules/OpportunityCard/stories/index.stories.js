import React from "react";
import OpportunityCard from "../";

export default {
  title: "Molecules/OpportunityCard",
  component: OpportunityCard,
};

const Template = () => (
  <div>
    <OpportunityCard />
  </div>
);

export const Default = Template.bind({});
