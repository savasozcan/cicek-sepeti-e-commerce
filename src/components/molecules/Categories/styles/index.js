import styled from "styled-components";

export const StyledCategoryWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;

  @media (max-width: 1024px) {
    flex-wrap: nowrap;
    overflow: scroll;
    overflow-x: auto;
  }
`;

export const StyledButtonWrapper = styled.div`
  padding: 0.5rem;
  width: 195px;
  height: 50px;
  margin: 5px 6px 15px 6px;
  cursor: pointer;
`;

export const StyledWrapper = styled.div`
  padding: 0 12px;
`;

export const StyledTitleWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export default {
  styledButton: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "195px",
    height: "50px",
  },
};

export const StyledCategoryTitle = styled.span`
  font-family: "Source Sans Pro", sans-serif;
  font-size: ${(props) => props.theme.sizes.large};
  font-weight: ${(props) => props.theme.fonts.weights.regular};
  padding-left: 10px;
  color: ${(props) => props.theme.colors.blue};
`;
