import React from "react";
import Category from "../index";
import { Container, Row, Col } from "react-bootstrap";

export default {
  title: "Molecules/Category",
  component: Category,
};

const Template = (args) => (
  <Container>
    <Row>
      <Col>
        <Category {...args} />
      </Col>
    </Row>
  </Container>
);

export const Default = Template.bind({});
Default.args = {};
