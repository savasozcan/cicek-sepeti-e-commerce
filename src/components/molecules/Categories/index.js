import React, { useEffect } from "react";
import { connect } from "react-redux";
import Button from "../../atoms/Button";
import Image from "../../atoms/Image";
import style, {
  StyledWrapper,
  StyledCategoryWrapper,
  StyledButtonWrapper,
  StyledCategoryTitle,
  StyledTitleWrapper,
} from "./styles";
import categoryIcon from "../../../assets/images/category-icon.svg";
import data from "./mock";
import { getCategories } from "../../../redux/actions/categoryActions";
import getFilter from "../../../redux/actions/filterActions";
import Breadcrumb from "../Breadcrumb";

const Category = (props) => {
  const categories = props.categories.categoryList;

  useEffect(() => {
    props.getCategories();
  });

  const handleFilter = (filter) => {
    props.getFilter(filter);
  };

  return (
    <StyledWrapper>
      <Breadcrumb />
      <StyledTitleWrapper>
        <Image src={categoryIcon} />
        <StyledCategoryTitle>Kategoriler</StyledCategoryTitle>
      </StyledTitleWrapper>
      <StyledCategoryWrapper>
        <StyledButtonWrapper>
          <Button
            style={{ ...style.styledButton }}
            use="categoryAll"
            onClick={() => handleFilter("")}
          >
            Tümünü Gör
          </Button>
        </StyledButtonWrapper>
        {categories.map((item, i) => (
          <StyledButtonWrapper>
            <Button
              style={{ ...style.styledButton }}
              use="categoryGhost"
              key={i}
              onClick={() => handleFilter(item.id)}
            >
              {item.label}
            </Button>
          </StyledButtonWrapper>
        ))}
      </StyledCategoryWrapper>
    </StyledWrapper>
  );
};

const mapStateToProps = (state) => ({
  categories: state.categoryReducer,
  filter: state.filterReducer,
});

const mapDispatchToProps = {
  getCategories,
  getFilter,
};

export default connect(mapStateToProps, mapDispatchToProps)(Category);
