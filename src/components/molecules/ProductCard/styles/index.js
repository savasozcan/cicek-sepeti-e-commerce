import styled from "styled-components";
import Button from "../../../atoms/Button";

export const StyledWrapper = styled.div`
  display: flex;
  box-sizing: border-box;
  padding: ${(props) => props.theme.sizes.small};
  width: 272px;
  height: auto;
  margin-left: ${(props) => props.theme.fontSizes.size10.size};
  margin-top: ${(props) => props.theme.fontSizes.size10.size};
  transition: all 0.5s;
  flex-direction: column;
  background: ${(props) => props.theme.colors.white} 0% 0% no-repeat padding-box;
  border: 1px solid #e2e7e9;
  border-radius: ${(props) => props.theme.fontSizes.size10.size};
  font-family: "Source Sans Pro", sans-serif;
  cursor: pointer;

  &:hover {
    box-shadow: 1px 1px 7px rgba(0, 0, 0, 0.4) !important;
  }
`;
export const StyledImageWrapper = styled.div``;
export const StyledInfoWrapper = styled.div``;
export const StyledButtonWrapper = styled.div`
  margin-top: ${(props) => props.theme.sizes.xxsmall};

  button {
    display: block;
    width: 100%;
  }
`;

export const StyledTitle = styled.div`
  color: ${(props) => props.theme.colors.greyDark};
  font-size: ${(props) => props.theme.sizes.small};
  font-weight: ${(props) => props.theme.fonts.weights.semiBold};
  margin-bottom: ${(props) => props.theme.sizes.xxsmall};
  min-height: 40px;
  max-height: 40px;
`;

export const StyledDelivery = styled.div`
  color: ${(props) => props.theme.colors.green};
  font-size: ${(props) => props.theme.sizes.small};
  font-weight: ${(props) => props.theme.fonts.weights.semiBold};
  margin-bottom: ${(props) => props.theme.sizes.xxsmall};
  min-height: 22px;
`;

export const StyledPrice = styled.span`
  color: ${(props) => props.theme.colors.black};
  font-weight: ${(props) => props.theme.fonts.weights.semiBold};
  font-size: ${(props) => props.theme.sizes.medium};
  margin-bottom: ${(props) => props.theme.sizes.xxsmall};
`;

export const StyledCounterWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: 2px;
  padding-right: 2px;
  width: 100%;
  height: 30px;
  border: 1px solid #e2e7e9;
  border-radius: 25px;
`;

export const StyledIncreaseButton = styled(Button)`
  width: 25px;
  height: 25px;
  margin-right: 10px;
  text-align: right;

  img {
    width: 20px;
    height: 20px;
  }
`;

export const StyledDecreaseButton = styled(Button)`
  width: 25px;
  height: 25px;
  margin-left: 10px;
  text-align: left;

  img {
    width: 20px;
    height: 20px;
  }
`;

export const StyledCount = styled.span`
  color: ${(props) => props.theme.colors.greyDark};
  font-size: ${(props) => props.theme.sizes.xmedium};
  font-weight: ${(props) => props.theme.fonts.weights.semiBold};
`;
