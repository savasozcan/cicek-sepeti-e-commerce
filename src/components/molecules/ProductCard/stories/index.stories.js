import React from "react";
import ProductCard from "../";
import { Container, Row, Col } from "react-bootstrap";

const mockProduct = {
  id: 8,
  image: "/images/category_8.png",
  content: "Özel Tatlar Çikolata Kutusu 300 gr",
  categoryId: 8,
  categoryName: "Hediye",
  delivery: "Ücretsiz Teslimat",
  price: 59.9,
};

export default {
  title: "Molecules/ProductCard",
  component: ProductCard,
};

const Template = () => (
  <Container>
    <Row>
      <Col xxl="3" xl="4" lg="6" md="8" sm="8" xs="12">
        <ProductCard product={mockProduct} />
      </Col>
    </Row>
  </Container>
);

export const Default = Template.bind({});
