import React, { useState, useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import { addToBasket } from "../../../redux/actions/basketActions";
import {
  StyledWrapper,
  StyledImageWrapper,
  StyledInfoWrapper,
  StyledButtonWrapper,
  StyledTitle,
  StyledDelivery,
  StyledPrice,
  StyledCounterWrapper,
  StyledIncreaseButton,
  StyledDecreaseButton,
  StyledCount,
} from "./styles";
import Button from "../../atoms/Button";
import enums from "./enums";
import Image from "../../atoms/Image";
import decrease from "../../../assets/images/decrease.svg";
import increase from "../../../assets/images/increase.svg";

const ProductCard = (props) => {
  const dispatch = useDispatch();
  const { product, index } = props;
  const basket = props.basket.basket;
  const [isCounterShow, setIsCounterShow] = useState(false);
  const [count, setCounter] = useState(1);

  const addToBasket = () => {
    props.addToBasket(product);
  };

  const removeProduct = () => {
    setCounter(count - 1);
    dispatch({
      type: "REMOVE_FROM_BASKET",
      payload: index,
    });
    if (count <= 1) setCounter(1);
  };

  const addProduct = () => {
    setCounter(count + 1);
    addToBasket();
  };

  useEffect(() => {
    const isSelect = basket.some((product) => product.id === props.id);
    setIsCounterShow(isSelect);
  }, [basket, product.id, props.id]);

  return (
    <>
      <StyledWrapper>
        <StyledImageWrapper>
          <img src={product.image} alt={product.content} />
        </StyledImageWrapper>
        <StyledInfoWrapper>
          <StyledTitle>{product.content}</StyledTitle>
          <StyledDelivery>{product.delivery}</StyledDelivery>
          <StyledPrice>{product.price} TL</StyledPrice>
        </StyledInfoWrapper>
        <StyledButtonWrapper>
          {!isCounterShow ? (
            <Button onClick={addToBasket} use="ghost" shape="pill">
              {enums.ADD_TO_BASKET}
            </Button>
          ) : (
            <StyledCounterWrapper>
              <StyledDecreaseButton onClick={removeProduct}>
                <Image src={decrease} />
              </StyledDecreaseButton>
              <StyledCount>{count}</StyledCount>
              <StyledIncreaseButton onClick={addProduct}>
                <Image src={increase} />
              </StyledIncreaseButton>
            </StyledCounterWrapper>
          )}
        </StyledButtonWrapper>
      </StyledWrapper>
    </>
  );
};

const mapStateToProps = (state) => ({
  basket: state.basketReducer,
});

const mapDispatchToProps = (dispatch) => {
  return {
    addToBasket: (product) => {
      dispatch(addToBasket(product));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductCard);
