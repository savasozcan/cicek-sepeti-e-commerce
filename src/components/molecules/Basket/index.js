import React, { useEffect, useState } from "react";

import Button from "../../atoms/Button";
import enums from "./enums";
import style, { StyledWrapper, StyledCounter } from "./styles";

const Basket = ({ items }) => {
  const [basketItems, setBasketItems] = useState(null);

  useEffect(() => {
    setBasketItems(items.length);
    console.log("items => ", items);
  }, [items]);

  return (
    <StyledWrapper>
      <Button
        style={{ ...style.buttonStyle }}
        prefixIcon="shopping-cart"
        use="basket"
        shape="pill"
      >
        {enums.BASKET_BUTTON_TEXT}
      </Button>
      {basketItems > 0 && <StyledCounter>{basketItems}</StyledCounter>}
    </StyledWrapper>
  );
};

export default Basket;
