import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Basket from "../";

export default {
  title: "Molecules/Basket",
  component: Basket,
};

const Template = () => (
  <Container>
    <Row>
      <Col>
        <Basket />
      </Col>
    </Row>
  </Container>
);

export const Default = Template.bind({});
