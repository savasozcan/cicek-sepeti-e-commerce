import styled from "styled-components";

export const StyledWrapper = styled.div`
  position: relative;
`;

export const StyledCounter = styled.span`
  display: inline-flex;
  position: absolute;
  top: -${(props) => props.theme.fontSizes.size16.size};
  right: -${(props) => props.theme.sizes.xxxsmall};
  -webkit-box-align: center;
  align-items: center;
  -webkit-box-pack: center;
  justify-content: center;
  border-radius: 50%;
  background-color: rgb(230, 32, 72);
  padding: ${(props) => props.theme.fontSizes.size12.size};
  width: ${(props) => props.theme.fontSizes.size16.size};
  height: ${(props) => props.theme.fontSizes.size16.size};
  color: rgb(255, 255, 255);
  font-size: ${(props) => props.theme.fontSizes.size16.size};
  font-weight: ${(props) => props.theme.fonts.weights.bold};
`;

export default {
  buttonStyle: {
    display: "flex",
  },
};
