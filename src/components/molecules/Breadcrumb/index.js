import React from "react";
import Icon from "../../atoms/Icon";
import {
  StyledWrapper,
  StyledItemWrapper,
  StyledActiveItem,
  StyledItem,
} from "./styles";

const Breadcrumb = () => {
  return (
    <StyledWrapper>
      <StyledItemWrapper>
        <StyledItem>Ciceksepeti Market</StyledItem>
        <Icon name="chevron-right" />
      </StyledItemWrapper>
      <StyledItemWrapper>
        <StyledItem>Istanbul</StyledItem>
        <Icon name="chevron-right" />
      </StyledItemWrapper>
      <StyledItemWrapper>
        <StyledActiveItem>CiceksepetiBreadCrumb</StyledActiveItem>
      </StyledItemWrapper>
    </StyledWrapper>
  );
};

export default Breadcrumb;
