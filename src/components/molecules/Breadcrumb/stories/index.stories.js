import React from "react";
import Breadcrumb from "../index";
import { Container, Row, Col } from "react-bootstrap";

export default {
  title: "Molecules/Breadcrumb",
  component: Breadcrumb,
};

const Template = (args) => (
  <Container>
    <Row>
      <Col>
        <Breadcrumb {...args} />
      </Col>
    </Row>
  </Container>
);

export const Default = Template.bind({});
Default.args = {};
