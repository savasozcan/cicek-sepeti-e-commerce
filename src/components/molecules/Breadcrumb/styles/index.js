import styled from "styled-components";

export const StyledWrapper = styled.div`
  display: flex;
  font-family: "Source Sans Pro", sans-serif;
  font-size: ${(props) => props.theme.sizes.xsmall};
  align-items: center;
  margin-top: 10px;

  svg {StyledActiveItem
    width: 13px;
    height: 13px;
  }
`;

export const StyledItemWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const StyledItem = styled.div`
  color: ${(props) => props.theme.colors.greyDark}
`;

export const StyledActiveItem = styled.div`
  color: ${(props) => props.theme.colors.green};
`;
