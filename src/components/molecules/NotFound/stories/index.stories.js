/* eslint-disable react/style-prop-object */
import React from "react";
import NotFound from "../index";

// This default export determines where your story goes in the story list
export default {
  title: "Molecules/NotFound",
  component: NotFound,
};

const Template = (args) => <NotFound {...args} icon="frown" text="Aradığınız Sonuç Bulunamadı" />;

export const Default = Template.bind({});
Default.args = {};
