import styled from "styled-components";

export const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 10px;
`;

export const StyledText = styled.span`
  font-family: "Noto Sans JP", sans-serif;
  font-size: ${(props) => props.theme.fontSizes.size18.size};
  color: ${(props) => props.theme.colors.greyDark};
`;

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  iconStyle: {
    width: "50px",
    height: "50px",
    marginBottom: "10px",
    color: "rgba(255, 114, 94, 1)",
  },
};
