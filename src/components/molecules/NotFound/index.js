import React from "react";
import Icon from "../../atoms/Icon";
import style, { StyledWrapper, StyledText } from "./styles";

const NotFound = ({ icon, text }) => {
  return (
    <StyledWrapper>
      <Icon style={{ ...style.iconStyle }} name={icon} />
      <StyledText>{text}</StyledText>
    </StyledWrapper>
  );
};

export default NotFound;
