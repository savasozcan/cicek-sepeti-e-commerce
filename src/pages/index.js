import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import ProductCard from "../components/molecules/ProductCard";
import OpportunityCard from "../components/molecules/OpportunityCard";
import Header from "../components/organisms/Header";
import { Container, Row, Col } from "styled-bootstrap-grid";
import { getProducts } from "../redux/actions/productActions";
import DiscountProgress from "../components/molecules/DiscountProgress";
import NotFound from "../components/molecules/NotFound";
import Category from "../components/molecules/Categories";
import { StyledContainer } from "../styleGuide/GlobalStyle";
import Image from "../components/atoms/Image";
import categoryIcon from "../assets/images/category-title.svg";
import Footer from "../components/organisms/Footer";
import FooterCopyRight from "../components/molecules/Footer/FooterCopyRight";
import FooterMain from "../components/molecules/Footer/FooterMain";
import FooterBottomInfo from "../components/molecules/Footer/FooterBottomInfo";
import contentData from "../data";

const StyledWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
  grid-column-gap: 20px;
  grid-row-gap: 20px;
  margin-bottom: 30px;
`;

const StyledOpportunityWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  margin: 0;
  padding: 0 0 100px 0;
  overflow-x: auto;
  text-align: center;
  scroll-behavior: smooth;
`;

const StyledTitle = styled.div`
  display: flex;
  align-items: center;
  color: ${(props) => props.theme.colors.greyLight};
  font-size: ${(props) => props.theme.sizes.xmedium};
  margin-top: 20px;

  span {
    padding-left: 5px;
  }
`;

export const StyledFooterWrapper = styled.div`
  width: 1480px;
  margin: 0 auto;
  padding-top: 100px;

  @media (max-width: 1440px) {
    width: 1140px;
  }
`;

const Content = (props) => {
  const { products } = props;

  useEffect(() => {
    props.getProducts();
  });

  const { filter } = props;

  const search = props.search !== "" ? props.search.toLowerCase() : "";

  const isExist = props.products.some(
    (product) => product.content.toLowerCase().indexOf(search) !== -1
  );

  const productListData = products
    .filter((product) => {
      if (search === "") return product;
      else if (product?.content?.toLowerCase().indexOf(search) >= 0)
        return product;
    })
    .filter((product) => {
      if (!filter) return product;
      else if (filter === product.categoryId) return product;
    });

  return (
    <>
      <Header />
      <StyledContainer>
        <Row>
          <Category />
          <Col>
            {!isExist && (
              <NotFound
                icon="frown"
                text={`${search} aramanız için sonuç bulunamadı...`}
              />
            )}
          </Col>
          <Col>
            <StyledTitle>
              <Image src={categoryIcon} alt="category" />
              <span>Tüm Kategoriler</span>
            </StyledTitle>
            <StyledWrapper>
              {productListData.map((item, i) => (
                <ProductCard product={item} id={item.id} index={i} />
              ))}
            </StyledWrapper>
          </Col>
          <Col>
            <StyledOpportunityWrapper>
              {contentData.banner.map((item, i) => (
                <OpportunityCard data={item} />
              ))}
            </StyledOpportunityWrapper>
          </Col>
        </Row>
      </StyledContainer>
      <div style={{ background: "#F2F2F2" }}>
        <StyledFooterWrapper>
          <Footer />
          <FooterMain />
          <FooterBottomInfo />
        </StyledFooterWrapper>
      </div>
      <FooterCopyRight />
    </>
  );
};

const mapStateToProps = (state) => ({
  products: state.productsReducer,
  search: state.searchReducer,
  filter: state.filterReducer,
});

const mapDispatchToProps = {
  getProducts,
};

export default connect(mapStateToProps, mapDispatchToProps)(Content);
