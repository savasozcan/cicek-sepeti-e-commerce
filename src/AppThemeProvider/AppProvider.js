/* eslint-disable react/prop-types */
import React from "react";
import { ThemeProvider } from "styled-components";
import theme from "../styleGuide/globalTheme";

const AppThemeProvider = ({ initialTheme, children }) => (
  <ThemeProvider theme={{ ...theme, ...initialTheme }}>
    {children}
  </ThemeProvider>
);

export default AppThemeProvider;
