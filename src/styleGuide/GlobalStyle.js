import styled, { createGlobalStyle } from "styled-components";
import { theme } from "../styleGuide";
import { Container } from "styled-bootstrap-grid";

export default createGlobalStyle`
  body {
    margin: 0;
    background: ${theme.colors.background};
    color: ${theme.colors.textColor};
    font-family: 'Noto Sans JP', sans-serif;
  }
  button{
    font-family: 'Noto Sans JP', sans-serif;
    font-size: 1em;
  }
  .header{
    grid-area: header;
  }
  .sidebar{
    grid-area: sidebar;
  }
  .main{
    grid-area: main;
  }
  .aside{
    grid-area: aside;
    overflow: auto;
  }
  footer{
    grid-area: footer;
  }
  .section{
    grid-area: section;
  }    
`;

export const StyledContainer = styled(Container)`
  @media (min-width: 1441px) {
    max-width: 1480px;
  }
`;
