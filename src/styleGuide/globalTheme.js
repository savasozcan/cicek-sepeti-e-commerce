import gridSettings from "./gridSettings";
export default {
  gridGap: 1, //rem
  device: {
    xs: `(max-width: ${gridSettings.breakpoints.xs}px)`,
    sm: `(min-width: ${gridSettings.breakpoints.sm}px)`,
    md: `(min-width: ${gridSettings.breakpoints.md}px)`,
    lg: `(min-width: ${gridSettings.breakpoints.lg}px)`,
    xl: `(min-width: ${gridSettings.breakpoints.xl}px)`,
    xxl: `(min-width: ${gridSettings.breakpoints.xxl}px)`,
  },
  fonts: {
    family: {
      muli: `'Muli', sans-serif`,
    },
    weights: {
      light: "300",
      regular: "400",
      medium: "500",
      semiBold: "600",
      bold: "700",
      extraBold: "800",
    },
  },
  colors: {
    primary: "rgba(255, 114, 94, 1)",
    primary90: "rgba(255, 114, 94, 0.9)",
    primary80: "rgba(255, 114, 94, 0.8)",
    primary70: "rgba(255, 114, 94, 0.7)",
    primary60: "rgba(255, 114, 94, 0.6)",
    primary50: "rgba(255, 114, 94, 0.5)",
    primary40: "rgba(255, 114, 94, 0.4)",
    primary30: "rgba(255, 114, 94, 0.3)",
    primary20: "rgba(255, 114, 94, 0.2)",
    primary10: "rgba(255, 114, 94, 0.1)",

    primaryGreyLight: "rgba(253, 114, 95, 0.08)",

    primaryGrey: "#f7f5f5",
    greyExtraDark: "#868585",
    white: "#FFFFFF",
    superWhite: "#FFFFFF",
    black: "#000000",
    greyDark: "#555555",
    grey: "#B3B3B3",
    greyLight: "#707070",
    greyLighter: "#E6E6E6",
    headerBg: "#F8EEEA",
    warning: "#FFCA28",
    lightWarning: "#f8eac4",
    greyLightest: "#637381",
    blue: "#044DC3",
    green: "#51b549",
    brown: "#E64E41",
    lightGrey: "#e2e7e9",
    red: "#F54257",
  },
  sizes: {
    xxxsmall: "4px",
    xxsmall: "8px",
    xsmall: "12px",
    small: "14px",
    medium: "16px",
    xmedium: "20px",
    large: "24px",
    xlarge: "28px",
    xxlarge: "32px",
    xxxlarge: "36px",
  },
  fontSizes: {
    size10: {
      size: "10px",
      lineHeight: "normal",
    },
    size12: {
      size: "12px",
      lineHeight: "1.5",
    },
    size14: {
      size: "14px",
      lineHeight: "20px",
    },
    size16: {
      size: "16px",
      lineHeight: "24px",
    },
    size18: {
      size: "18px",
      lineHeight: "normal",
    },
  },
  input: {
    height: "36px",

    addon: {
      height: "34px",
    },
  },

  button: {
    height: "36px",
  },
};
