import React from "react";
import "./App.css";
import { ThemeProvider } from "styled-components";
import { Route } from "react-router-dom";
import { gridSettings, theme, GlobalStyle } from "./styleGuide";
import { GridThemeProvider, BaseCSS } from "styled-bootstrap-grid";

//Components
import Content from "../src/pages";

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <GridThemeProvider gridTheme={gridSettings}>
        <GlobalStyle />
        <BaseCSS />
        <div>
          <Content />
          {/*  <Route path="/basket" component={Basket} />*/}
        </div>
      </GridThemeProvider>
    </ThemeProvider>
  );
};

export default App;
