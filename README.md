## Initialization

### `yarn install`

## Available Scripts

In the project directory, you can run:

### `yarn start`
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Run the database:

### `npx json-server --watch src/data/db.json --port 8000`
Open [http://localhost:3000](http://localhost:8000) to view it in the browser.
### Related urls
Products: http://localhost:8000/products 
Categories: http://localhost:8000/categories


